Тестовая задачка от СКБ Контур на Симметрии.

# Суть задачи
Необходимо найти вертикальную ось симметрии для множества точек
такую чтобы она пересекала ось X в точке с целочисленными координатами
если такой прямой нет - вернуть null.

Все точки в массиве уникальны (нет точек с одинаковыми координатами). Для проверки решения необходимо запустить тесты.

# Объяснение задачи
Нужно найти зеркальную симметрию по вертикали, либо вернуть null если не удалось ее найти. 
Несколько визуальных примеров:

![kontur1.png](https://gitlab.com/23Alex24/interview.skbcontur.symmetry/raw/master/screenshots/kontur1.png)

![kontur2.png](https://gitlab.com/23Alex24/interview.skbcontur.symmetry/raw/master/screenshots/kontur2.png)

![kontur3.png](https://gitlab.com/23Alex24/interview.skbcontur.symmetry/raw/master/screenshots/kontur3.png)