﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interview.SkbContur.Symmetry.RdpVariant2
{
    public class SplitPointsResult
    {
        /// <summary>
        /// Points with mirror symmetry
        /// </summary>
        public IList<Tuple<Point, Point>> MirrorPairs { get; set; }

        /// <summary>
        /// points without mirror symmetry
        /// </summary>
        public IList<Point> NotPairsPoints { get; set; }
    }
}
