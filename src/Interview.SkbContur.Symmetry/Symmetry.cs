﻿using Interview.SkbContur.Symmetry.GoodVariant;

namespace Interview.SkbContur.Symmetry
{
    /* Задача:
            * Необходимо найти вертикальную ось симметрии для множества точек
            *  такую чтобы она пересекала ось X в точке с целочисленными координатами
            *  если такой прямой нет - вернуть null
            * 
            * Все точки в массиве уникальны (нет точек с одинаковыми координатами)
            * 
            * для проверки решения необходимо запустить тесты
     *
     * Задание означает найти зеркальную симметрию (проверить есть ли она) по вертикальной оси
            */
    public class VerticalAxisOfSymmetry
    {
        public int? GetX(params Point[] points)
        {
            return GoodVariant1.Execute(points);
        }
    }    
}
