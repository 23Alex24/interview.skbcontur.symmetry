﻿using System.Collections;
using NUnit.Framework;

namespace Interview.SkbContur.Symmetry.Tests
{
    [TestFixture]
    public class Tests
    {
        private static IEnumerable TestCasesWithAxis
        {
            get
            {
                yield return new TestCaseData(123, new[] { new Point(123, 456) }).SetName("one point");
                yield return new TestCaseData(15, new[] { new Point(10, 100), new Point(20, 100) }).SetName("two points");
                yield return new TestCaseData(9, new[] { new Point(9, 44), new Point(9, 55), new Point(9, -6) }).SetName("three points vertical");
                yield return new TestCaseData(4, new[] { new Point(2, 100), new Point(6, 100), new Point(4, 100) }).SetName("three points on line");
                yield return new TestCaseData(3, new[] { new Point(2, 100), new Point(3, 0), new Point(4, 100) }).SetName("three points triangle");
                yield return new TestCaseData(3, new[] { new Point(2, 100), new Point(4, 100), new Point(3, 0), new Point(3, 1000) }).SetName("for points");
                yield return new TestCaseData(3, new[] { new Point(2, 100), new Point(4, 100), new Point(3, 0), new Point(3, 1000), new Point(1, 200), new Point(5, 200) }).SetName("six points");
                //Данный тест был добавлен после первой RPD сессией
                yield return new TestCaseData(-8, new[] { new Point(-10, -100), new Point(-6, -100), new Point(-8, -20) }).SetName("negative");
                //Данный тест добавил я после второй RDP сессии и после того, как понял, что не все кейсы были предусмотрены тестами
                yield return new TestCaseData(25, new[] { new Point(10, 100), new Point(20, 100), new Point(40, 100),  new Point(20, 100), new Point(30, 100) }).SetName("Some mirror point on one line");
                //Я добавил
                yield return new TestCaseData(60, new[] { new Point(-10, 100), new Point(130, 100) }).SetName("two points with negative");
            }
        }

        private static IEnumerable TestCasesWithoutAxis
        {
            get
            {
                yield return new TestCaseData(new Point[0] as object).SetName("no points");
                yield return new TestCaseData(new[] { new Point(-1, -1), new Point(1, 1) } as object).SetName("two points");
                yield return new TestCaseData(new[] { new Point(0, 100), new Point(1, 100), new Point(10, 100) } as object).SetName("three points");
                yield return new TestCaseData(new[] { new Point(10, 0), new Point(11, 0) } as object).SetName("fraction 1");
                yield return new TestCaseData(new[] { new Point(10, 0), new Point(13, 0) } as object).SetName("fraction 3");
                //Данный тест добавил я, чтобы проверить ситуацию, когда среднее у трех точек есть целое, но оно не является симметрией
                yield return new TestCaseData(new[] { new Point(1, 0), new Point(2, 0), new Point(9, 0) } as object).SetName("fraction 3");
            }
        }

        [TestCaseSource(nameof(TestCasesWithAxis))]
        public void TestWithAxis(int expectedX, Point[] points)
        {
            var actualX = new VerticalAxisOfSymmetry().GetX(points);
            Assert.That(actualX, Is.EqualTo(expectedX));
        }

        [TestCaseSource(nameof(TestCasesWithoutAxis))]
        public void TestWithoutAxis(Point[] points)
        {
            var actualX = new VerticalAxisOfSymmetry().GetX(points);
            Assert.That(actualX, Is.Null);
        }
    }
}
